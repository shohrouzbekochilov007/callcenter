const mongoose = require('mongoose')
const Joi = require("joi")

const TestSchema = new mongoose.Schema({
    test_decription: {
        type: String,
        required: true
    }
});

function validateTest(user) {
    
    const schema = Joi.object({
        question_text: Joi.string().required(),
        updated_at: Joi.date(),
        before_id: Joi.string(),
    });

    return schema.validate(user);
}

const Test = mongoose.model("tests", TestSchema);
module.exports.Test = Test;
module.exports.validateTest = validateTest;