const mongoose = require('mongoose')
const Joi = require("joi")

const FlowSchema = new mongoose.Schema({
    question_text: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: new Date()
    },
    before_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "flow",
        default: null
    },
    updated_at: {
        type: Date,
        default: new Date()
    }
});

function validateFlow(user) {
    
    const schema = Joi.object({
        question_text: Joi.string().required(),
        updated_at: Joi.date(),
        before_id: Joi.string(),
    });

    return schema.validate(user);
}

const Flow = mongoose.model("flows", FlowSchema);
module.exports.Flow = Flow;
module.exports.validateFlow = validateFlow;