const express = require('express')
const router = express.Router()
const _ = require('lodash')
const { auth } = require('./../middleware/auth')


router.get('/main', auth, async (req, res) => {
    return res.render("main", {

    })
})

router.get('/tests', auth, async (req, res) => {
    return res.render("main", {

    })
})


module.exports = router;